// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBSiVmISjqkEGs8m-NixXGUwleVuEoRlFg",
    authDomain: "piko-app-326cd.firebaseapp.com",
    projectId: "piko-app-326cd",
    storageBucket: "piko-app-326cd.appspot.com",
    messagingSenderId: "623997637317",
    appId: "1:623997637317:web:b7148cdb3c85af64ae96ac",
    measurementId: "G-87MQE3XGDJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
