import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { EntryGuard } from './common/guard/entry.guard';
import { ExitGuard } from './common/guard/exit.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule),
    // canActivate : [ExitGuard]
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/authentication/signup/signup.module').then( m => m.SignupPageModule),
    // canActivate : [EntryGuard]
  },
  // {
  //   path: '',
  //   loadChildren: () => import('./pages/authentication/login/login.module').then( m => m.LoginPageModule)
  // },
  {
    path: 'login',
    loadChildren: () => import('./pages/authentication/login/login.module').then( m => m.LoginPageModule),
    // canActivate : [EntryGuard]
  },
  {
    path: 'video-details',
    loadChildren: () => import('./pages/video-details/video-details.module').then( m => m.VideoDetailsPageModule)
  },
  {
    path: 'profile-info',
    loadChildren: () => import('./pages/profile-info/profile-info.module').then( m => m.ProfileInfoPageModule)
  },
  {
    path: 'activity',
    loadChildren: () => import('./pages/activity/activity.module').then( m => m.ActivityPageModule)
  },
  {
    path: 'my-business',
    loadChildren: () => import('./pages/my-business/my-business.module').then( m => m.MyBusinessPageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./pages/chat/chat.module').then( m => m.ChatPageModule)
  },
  {
    path: 'group-list',
    loadChildren: () => import('./pages/group-list/group-list.module').then( m => m.GroupListPageModule)
  },
  {
    path: 'following-request',
    loadChildren: () => import('./pages/following-request/following-request.module').then( m => m.FollowingRequestPageModule)
  },
  {
    path: 'followers',
    loadChildren: () => import('./pages/followers/followers.module').then( m => m.FollowersPageModule)
  },
  {
    path: 'following',
    loadChildren: () => import('./pages/following/following.module').then( m => m.FollowingPageModule)
  },
  {
    path: 'favorite-videos',
    loadChildren: () => import('./pages/favorite-videos/favorite-videos.module').then( m => m.FavoriteVideosPageModule)
  },
  {
    path: 'group-details',
    loadChildren: () => import('./pages/group-details/group-details.module').then( m => m.GroupDetailsPageModule)
  },
  {
    path: 'list-of-setting',
    loadChildren: () => import('./pages/settings/list-of-setting/list-of-setting.module').then( m => m.ListOfSettingPageModule)
  },
  {
    path: 'more-settings',
    loadChildren: () => import('./pages/settings/more-settings/more-settings.module').then( m => m.MoreSettingsPageModule)
  },
  {
    path: 'privacy-policy',
    loadChildren: () => import('./pages/settings/privacy-policy/privacy-policy.module').then( m => m.PrivacyPolicyPageModule)
  },
  {
    path: 'terms-of-use',
    loadChildren: () => import('./pages/settings/terms-of-use/terms-of-use.module').then( m => m.TermsOfUsePageModule)
  },
  {
    path: 'cookie-policy',
    loadChildren: () => import('./pages/settings/cookie-policy/cookie-policy.module').then( m => m.CookiePolicyPageModule)
  },
  {
    path: 'privacy',
    loadChildren: () => import('./pages/settings/privacy/privacy.module').then( m => m.PrivacyPageModule)
  },
  {
    path: 'job-details',
    loadChildren: () => import('./pages/jobs/job-details/job-details.module').then( m => m.JobDetailsPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./pages/notification/notification.module').then( m => m.NotificationPageModule)
  },
  {
    path: 'universal-search',
    loadChildren: () => import('./pages/universal-search/universal-search.module').then( m => m.UniversalSearchPageModule)
  },
  {
    path: 'business-details',
    loadChildren: () => import('./pages/business/business-details/business-details.module').then( m => m.BusinessDetailsPageModule)
  },
  {
    path: 'business-inbox',
    loadChildren: () => import('./pages/business/business-inbox/business-inbox.module').then( m => m.BusinessInboxPageModule)
  },
  {
    path: 'mail-confirm',
    loadChildren: () => import('./pages/authentication/mail-confirm/mail-confirm.module').then( m => m.MailConfirmPageModule)
  },
  {
    path: 'choose-option',
    loadChildren: () => import('./pages/choose-option/choose-option.module').then( m => m.ChooseOptionPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
