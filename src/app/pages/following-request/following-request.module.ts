import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowingRequestPageRoutingModule } from './following-request-routing.module';

import { FollowingRequestPage } from './following-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FollowingRequestPageRoutingModule
  ],
  declarations: [FollowingRequestPage]
})
export class FollowingRequestPageModule {}
