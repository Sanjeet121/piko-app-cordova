import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FollowingRequestPage } from './following-request.page';

const routes: Routes = [
  {
    path: '',
    component: FollowingRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FollowingRequestPageRoutingModule {}
