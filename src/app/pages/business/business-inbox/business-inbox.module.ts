import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessInboxPageRoutingModule } from './business-inbox-routing.module';

import { BusinessInboxPage } from './business-inbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessInboxPageRoutingModule
  ],
  declarations: [BusinessInboxPage]
})
export class BusinessInboxPageModule {}
