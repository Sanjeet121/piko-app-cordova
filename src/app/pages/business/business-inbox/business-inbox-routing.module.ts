import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessInboxPage } from './business-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessInboxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessInboxPageRoutingModule {}
