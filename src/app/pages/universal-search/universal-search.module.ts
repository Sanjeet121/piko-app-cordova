import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UniversalSearchPageRoutingModule } from './universal-search-routing.module';

import { UniversalSearchPage } from './universal-search.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UniversalSearchPageRoutingModule
  ],
  declarations: [UniversalSearchPage]
})
export class UniversalSearchPageModule {}
