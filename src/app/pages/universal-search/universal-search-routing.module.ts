import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UniversalSearchPage } from './universal-search.page';

const routes: Routes = [
  {
    path: '',
    component: UniversalSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UniversalSearchPageRoutingModule {}
