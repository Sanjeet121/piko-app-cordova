import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/common/services/auth.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorService } from 'src/app/common/services/error.service';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  constructor(public authService : AuthService,
    public ngFireAuth: AngularFireAuth,
    public router : Router,
    public errorMessage : ErrorService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    ) { 
      this.signupForm = new FormGroup({
        'name' : new FormControl(null,[Validators.required,Validators.minLength(2)]),
        'email' : new FormControl(null,[Validators.required,Validators.email]),
        'password' : new FormControl(null, [Validators.required, Validators.minLength(8)]),
        'confirmPassword' : new FormControl(null, [Validators.required, Validators.minLength(8)])
      }, this.matchPassword)
    }

  ngOnInit() {
    
  }
  passwordType = false;
  passwordIcon = 'eye-off';
  changeIcon() {
    console.log("Icon Change");
    this.passwordType = ( this.passwordType ) ? false : true;
    this.passwordIcon = ( this.passwordType ) ? 'eye-outline' : 'eye-off';
  }
  passwordType1 = false;
  passwordIcon1 = 'eye-off';
  changeIcon1() {
    console.log("Icon Change");
    this.passwordType1 = ( this.passwordType1 ) ? false : true;
    this.passwordIcon1 = ( this.passwordType1 ) ? 'eye-outline' : 'eye-off';
  }
  matchPassword(group : FormGroup) : {[name: string] : boolean} {
    return (group.value.password === group.value.confirmPassword) ? null : {'passwordMatch' : false};
   }
  signupForm : FormGroup;

  signupUser(){
    console.log(this.signupForm);
  }

  onSignup(){
    this.showLoader('Please wait').then(()=>{
      this.authService.RegisterUser(this.signupForm.value.email.toLowerCase(),this.signupForm.value.password).then(result=>{
        console.log(result);
         this.ngFireAuth.currentUser.then(res=>{
           res.sendEmailVerification();
           res.updateProfile({displayName:this.signupForm.value.name})
          console.log(res);
          
          if(res.emailVerified){
            this.hideLoader();
            this.router.navigate([`/tabs`]);
          }else{
            this.hideLoader();
            this.router.navigate(['/mail-confirm']);
          }
        });
      }).catch(err=>{
        this.hideLoader();
        console.log(err);
        this.errorMessage.errorHandler(err);
      })
    }).catch(err=>{
      this.hideLoader();
      console.log(err);
      this.errorMessage.errorHandler(err);
    })
    
  }
  async successToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      color: "success",
      duration: 2000
    });
    toast.present();
  }

  async showLoader(msg: string) {
    const loading = await this.loadingController.create({
      message: msg,
      spinner: "bubbles",
      id : 'signup'
    });
    await loading.present();
  }

  hideLoader(){
    this.loadingController.dismiss({id : 'signup'});
  }
  private dismissLoader(){
    this.loadingController.getTop().then(loader =>{
      if(loader){
        this.loadingController.dismiss();
      }
    });
  }
}
