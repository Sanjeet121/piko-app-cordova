import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MailConfirmPageRoutingModule } from './mail-confirm-routing.module';

import { MailConfirmPage } from './mail-confirm.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MailConfirmPageRoutingModule
  ],
  declarations: [MailConfirmPage]
})
export class MailConfirmPageModule {}
