import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/common/services/auth.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { ErrorService } from 'src/app/common/services/error.service';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-mail-confirm',
  templateUrl: './mail-confirm.page.html',
  styleUrls: ['./mail-confirm.page.scss'],
})
export class MailConfirmPage implements OnInit {

  constructor(public authService : AuthService,
    public ngFireAuth: AngularFireAuth,
    public router : Router,
    public errorMessage : ErrorService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    ) { 
      this.ngFireAuth.currentUser.then(res=>{
        console.log(res);
      }).catch(err=>{
        console.log(err);
        this.errorMessage.errorHandler(err);
      })
    }

  ngOnInit() {
    
  }

  refresh(){

  }

  resendEmail(){
    this.ngFireAuth.currentUser.then(res=>{
      console.log(res);
      res.sendEmailVerification();
    }).catch(err=>{
      console.log(err);
      this.errorMessage.errorHandler(err);
    })
  }


}
