import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/common/services/auth.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { PikoStorageService } from 'src/app/common/services/piko-storage.service';
import { PKConstants } from 'src/app/common/constant/pkConstant';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorService } from 'src/app/common/services/error.service';
import { LoadingController, ToastController } from '@ionic/angular';
// import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  // loginForm : FormGroup;
  // constructor(public authService : AuthService,
  //   public ngFireAuth: AngularFireAuth,
  //   public afs : AngularFirestore,
  //   public router : Router,
  //   public storage : Storage,
  //   public pikoStorage : PikoStorageService,
  //   public errorMessage : ErrorService,
  //   public loadingController: LoadingController,
  //   public toastController: ToastController,
  //   // public facebook : Facebook
  //   ) {
  //     this.loginForm = new FormGroup({
  //       'email' : new FormControl(null,[Validators.required,Validators.email]),
  //       'password' : new FormControl(null,[Validators.required,Validators.minLength(8)])
  //     })
  //   }
  //   email:string='';
  //   password:string='';
  // ngOnInit() {

  // }
  // passwordType = false;
  // passwordIcon = 'eye-off';
  // changeIcon() {
  //   console.log("Icon Change");
  //   this.passwordType = ( this.passwordType ) ? false : true;
  //   this.passwordIcon = ( this.passwordType ) ? 'eye-outline' : 'eye-off';
  // }
  // onLogin(){
  //   this.showLoader('Please wait').then(()=>{
  //     this.authService.SignIn(this.loginForm.value.email.toLowerCase(),this.loginForm.value.password).then(result=>{
  //       console.log(result);

  //       if(result.user.emailVerified){
  //         this.hideLoader();
  //         this.storage.set(PKConstants.KEY_AUTH_ID,result.user.uid).then(id=>{
  //         console.log(id);
  //         this.pikoStorage.setValueInStorage(PKConstants.KEY_AUTH_ID,id);
  //           this.router.navigate(['/tabs']);
  //       })
  //       }else{
  //         this.hideLoader();
  //         this.router.navigate(['/mail-confirm']);
  //       }
  //     }).catch(err=>{
  //       this.hideLoader();
  //       console.log(err);
  //       this.errorMessage.errorHandler(err);
  //     })
  //   })

  // }
  // loginFacebook(){
  //   // this.facebook.login(['public_profile', 'user_friends', 'email'])
  //   // .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
  //   // .catch(e => console.log('Error logging into Facebook', e));
  //   // this.facebook.logEvent(this.facebook.EVENTS.EVENT_NAME_ADDED_TO_CART);
  // }

  // async successToast(msg: string) {
  //   const toast = await this.toastController.create({
  //     message: msg,
  //     color: "success",
  //     duration: 2000
  //   });
  //   toast.present();
  // }

  // async showLoader(msg: string) {
  //   const loading = await this.loadingController.create({
  //     message: msg,
  //     spinner: "bubbles",
  //     id:'login'
  //   });
  //   await loading.present();
  // }

  // hideLoader(){
  //   this.loadingController.dismiss({id:'login'});
  // }
  // private dismissLoader(){
  //   this.loadingController.getTop().then(loader =>{
  //     if(loader){
  //       this.loadingController.dismiss();
  //     }
  //   });
  // }

  constructor() { }

  ngOnInit() {
  }
}
