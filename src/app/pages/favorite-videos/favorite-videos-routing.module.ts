import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FavoriteVideosPage } from './favorite-videos.page';

const routes: Routes = [
  {
    path: '',
    component: FavoriteVideosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavoriteVideosPageRoutingModule {}
