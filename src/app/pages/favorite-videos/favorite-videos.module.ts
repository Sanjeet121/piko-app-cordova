import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FavoriteVideosPageRoutingModule } from './favorite-videos-routing.module';

import { FavoriteVideosPage } from './favorite-videos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FavoriteVideosPageRoutingModule
  ],
  declarations: [FavoriteVideosPage]
})
export class FavoriteVideosPageModule {}
