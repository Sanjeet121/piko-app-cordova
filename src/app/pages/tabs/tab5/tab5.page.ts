import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { PikoStorageService } from 'src/app/common/services/piko-storage.service';
import { AuthService } from 'src/app/common/services/auth.service';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {

  constructor(public router : Router,
    public storage : Storage,
    public pikoStorage : PikoStorageService,
    public authService :AuthService) { }

  ngOnInit() {
  }
  userLogout(){
    this.authService.SignOut();
  }
}
