import { Component, ElementRef, Renderer2, ViewChild, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  @ViewChild("header") header: HTMLElement;
  constructor(
    public element: ElementRef,
    public renderer: Renderer2
   ) {
  }
  ngOnInit() {
  }

  // in my case i'm using ionViewWillEnter
  ionViewWillEnter() {
    this.renderer.setStyle(this.header['el'], 'Transition', 'margin-top 400ms');
  }

  onContentScroll(event) {
    console.log("Event : " , event);
    if (event.detail.scrollTop >= 50) {
      this.renderer.setStyle(this.header['el'], 'margin-top', '-70px');
    } else {
      this.renderer.setStyle(this.header['el'], 'margin-top', '0px');
    }

  }
}
