import { Component, OnInit } from '@angular/core';
import { PKConstants } from 'src/app/common/constant/pkConstant';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  constructor(){}
  selectedTheme:boolean=false;
    ngOnInit(){
      if(localStorage.getItem(PKConstants.KEY_SELECTED_THEME) == 'dark'){
        this.selectedTheme=true;
      }else if(localStorage.getItem(PKConstants.KEY_SELECTED_THEME) == 'light'){
        this.selectedTheme = false;
      }else{
        this.selectedTheme=true;
      }
    }

    onToggleColorTheme(event){
       console.log(event.detail.checked);

       if(event.detail.checked){
          document.body.setAttribute('color-theme', 'dark');
          localStorage.setItem(PKConstants.KEY_SELECTED_THEME,'dark');
       }
       else{
        document.body.setAttribute('color-theme', 'light');
        localStorage.setItem(PKConstants.KEY_SELECTED_THEME,'light');
      }
    }
    }


