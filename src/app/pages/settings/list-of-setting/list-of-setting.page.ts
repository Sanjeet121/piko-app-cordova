import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { PikoStorageService } from 'src/app/common/services/piko-storage.service';
import { AuthService } from 'src/app/common/services/auth.service';
@Component({
  selector: 'app-list-of-setting',
  templateUrl: './list-of-setting.page.html',
  styleUrls: ['./list-of-setting.page.scss'],
})
export class ListOfSettingPage implements OnInit {


  constructor(public router : Router,
    public storage : Storage,
    public pikoStorage : PikoStorageService,
    public authService :AuthService) { }

  ngOnInit() {
  }
  userLogout(){
    this.authService.SignOut();
  }
}
