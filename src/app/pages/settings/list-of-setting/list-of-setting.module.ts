import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListOfSettingPageRoutingModule } from './list-of-setting-routing.module';

import { ListOfSettingPage } from './list-of-setting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListOfSettingPageRoutingModule
  ],
  declarations: [ListOfSettingPage]
})
export class ListOfSettingPageModule {}
