import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListOfSettingPage } from './list-of-setting.page';

const routes: Routes = [
  {
    path: '',
    component: ListOfSettingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListOfSettingPageRoutingModule {}
