import { Component } from '@angular/core';
import { PKConstants } from './common/constant/pkConstant';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(){
    this.toogleTheme();
  }
  toogleTheme(){
    if(localStorage.getItem(PKConstants.KEY_SELECTED_THEME) == 'light'){
      document.body.setAttribute('color-theme', 'light');
    }else if(localStorage.getItem(PKConstants.KEY_SELECTED_THEME) == 'dark'){
      document.body.setAttribute('color-theme', 'dark');
    }else{
      localStorage.setItem(PKConstants.KEY_SELECTED_THEME,'dark');
      document.body.setAttribute('color-theme', 'dark');
    }
  }
}
