import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PKConstants } from '../constant/pkConstant';
import { Storage } from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class EntryGuard implements CanActivate {
  constructor(private router: Router, private storage: Storage){}
  
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // console.log(next, state);
   
    return this.storage.get(PKConstants.KEY_AUTH_ID).then(value =>{
       console.log(value);

      if(value === null || value === undefined){
        return true;
      } else{
        this.router.navigate(['/tabs','tab1']);
        return false;
      }
    });
  } 
  
}
