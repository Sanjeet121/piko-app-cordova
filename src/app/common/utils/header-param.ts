export class HeaderParam{
    keyName:string;
    keyValue:string;
    constructor(keyname:string = undefined, keyvalue:string = undefined){
      if(keyname) this.keyName = keyname;
      if(keyvalue) this.keyValue = keyvalue;
    }
  }