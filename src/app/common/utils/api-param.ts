import { PKConstants } from '../constant/pkConstant';

export class ApiParam{
  url:string;
  resulttype: string;
  bodydata?: any;
  requesttype?: string = PKConstants.REQUEST_GET;
  errortype?: string = PKConstants.API_ERROR;
  activeLoader?: boolean = true;
  inputParams?: any;
  constructor(){};
}