import { Directive, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Storage } from '@ionic/storage';
import { ModalController, LoadingController, AlertController, PopoverController, ToastController, NavController, ActionSheetController } from '@ionic/angular';

import { DomSanitizer } from '@angular/platform-browser';
import { PikoStorageService } from '../services/piko-storage.service';
import { ApiService } from '../services/api.service';
import { AuthService } from '../services/auth.service';
import { ErrorService } from '../services/error.service';


@Directive()

export class BaseClass {

    constructor(
        public modalController: ModalController,
        public loadingController: LoadingController,
        public alertController: AlertController,
        public popoverController: PopoverController,
        public toastController: ToastController,
        public actionSheetController: ActionSheetController,
        public router: Router,
        public storage: Storage,
        public navController: NavController,
        public activeRoute: ActivatedRoute,
        public apiService: ApiService,
        public authService: AuthService,
        public errorService: ErrorService,
        public PikoStorage: PikoStorageService,
        public historyLocation: Location,
        public zone: NgZone,
        public sanitizer: DomSanitizer,
    ) {}
    public readonly defaultProfileImage : string = '/assets/images/default.jpeg';
    openInBrowser(url: string){
        const target = '_self';
        const options = 'location=yes,hideurlbar=yes,zoom=no,fullscreen=no,toolbarcolor=#4e24ac,navigationbuttoncolor=#ffffff,closebuttoncolor=#ffffff'
        // this.inAppBrowser.create(url, target, options);
    }
    async successToast(msg: string) {
        const toast = await this.toastController.create({
          message: msg,
          color: "success",
          duration: 2000
        });
        toast.present();
      }
    
      async showNotification(title:string,message:string) {
        const toast = await this.toastController.create({
          header: title,
          message: message,
          color: "primary",
          duration: 4000,
          position: "bottom",
          buttons: [
            {
              side: 'end',
              icon: 'close-circle',
              role: 'cancel'
            }
          ]
        });
        toast.present();
      }
}
