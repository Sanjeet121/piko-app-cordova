import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PikoStorageService {
  private _cmStorage:Map<string, string> = new Map<string, string>();

  constructor() { }

  public setValueInStorage(keyname:string, value:string):void{
      this._cmStorage.set(keyname, value);
  }

  public getValueFromStorage(keyname:string):string{
      return this._cmStorage.get(keyname);
  }

  public clearStorage():void{
      this._cmStorage.clear();
  }
}
