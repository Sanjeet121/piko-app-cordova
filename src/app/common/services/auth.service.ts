import { Injectable, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { ApiService } from './api.service';
import { Router } from '@angular/router';
import { ErrorService } from './error.service';
import { ToastController, LoadingController, NavController } from '@ionic/angular';
import { PikoStorageService } from './piko-storage.service';
import { AngularFireAuth } from "@angular/fire/compat/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { User } from '../shared/user';
import * as firebase from 'firebase/compat/app';
import { Storage } from '@ionic/storage';
declare var auth;
@Injectable({
  providedIn: 'root'
})
export class AuthService implements  OnInit, OnDestroy{
  private _apiSubscription:Subscription;
  constructor(private router: Router,
    private errorService: ErrorService, 
    private apiService: ApiService, 
    private toastController: ToastController, 
    private loadingController: LoadingController,
    private pikoStorage: PikoStorageService,
    private navController : NavController,
    public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth,
    public ngZone: NgZone,
    public storage : Storage 
   ) { 
    this._apiSubscription = this.apiService.apiResults.subscribe(data =>{
      if(data){

      }
    });
    this.ngFireAuth.authState.subscribe(user => {
      console.log("Auth State: ",user);
      // if (user) {
      //   this.userData = user;
      //   localStorage.setItem('user', JSON.stringify(this.userData));
      //   JSON.parse(localStorage.getItem('user'));
      // } else {
      //   localStorage.setItem('user', null);
      //   JSON.parse(localStorage.getItem('user'));
      // }
    })
   }
   ngOnInit(){
  }
  userData: any;

  

  // Login in with email/password
  SignIn(email, password) :Promise<any>{
    return new Promise((resolve,reject)=>{
      this.ngFireAuth.signInWithEmailAndPassword(email, password).then(res=>{
        console.log(res)
        resolve(res);
      }).catch(err=>{
        reject(err);
      })
    })
    
  }

  // Register user with email/password
  RegisterUser(email, password) :Promise<any>{
    return new Promise((resolve,reject)=>{
      this.ngFireAuth.createUserWithEmailAndPassword(email, password).then(res=>{
        resolve(res);
      }).catch(err=>{
        reject(err);
      })
    })
    
  }

  // Email verification when new user register
  SendVerificationMail() {
    // return this.ngFireAuth.res()
    // .then(() => {
    //   this.router.navigate(['verify-email']);
    // })
  }

  // Recover password
  PasswordRecover(passwordResetEmail) {
    return this.ngFireAuth.sendPasswordResetEmail(passwordResetEmail)
    .then(() => {
      window.alert('Password reset email has been sent, please check your inbox.');
    }).catch((error) => {
      window.alert(error)
    })
  }

  // Returns true when user is looged in
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }

  // Returns true when user's email is verified
  get isEmailVerified(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user.emailVerified !== false) ? true : false;
  }

  // Sign in with Gmail
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  // Auth providers
  AuthLogin(provider) {
    return this.ngFireAuth.signInWithPopup(provider)
    .then((result) => {
       this.ngZone.run(() => {
          // this.router.navigate(['dashboard']);
        })
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error)
    })
  }

  // Store user in localStorage
  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afStore.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  // Sign-out 
  SignOut() {
    return this.ngFireAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.pikoStorage.clearStorage();
      this.storage.clear();
      this.router.navigate(['login']);
    })
  }


  ngOnDestroy(){
    this._apiSubscription.unsubscribe();
  }
    


    async successToast(msg: string) {
      const toast = await this.toastController.create({
        message: msg,
        color: "success",
        duration: 2000
      });
      toast.present();
    }
  
    async showLoader(msg: string) {
      const loading = await this.loadingController.create({
        message: msg,
        spinner: "bubbles"
      });
      await loading.present();
    }
  
    hideLoader(){
      this.loadingController.dismiss();
    }
/**
 * End
 */

}
