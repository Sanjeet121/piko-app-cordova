import { Injectable } from '@angular/core';
import { PKConstants } from '../constant/pkConstant';
// import * as dev_url from '../config-data/dev_url.json';
//const prod_urls:any  = require('../data/test_data1.json');
const BASE_URL: string = "http://localhost:4200";
// const dev_urls:any = require("../config-data/dev-url.json");
// const dev_urls :string='';
@Injectable({
  providedIn: 'root'
})
export class UrlConverterService {

  private _isLocalEnv:boolean = true;
  private _stage:string = "dev";
  private _accessLevel: string = "public";

  constructor() { }

  public initialize(initGoogleMap: boolean = true):void{
    if(this._isLocalEnv){
      // PKConstants.AWS_S3_OBJECT_URL = `${dev_urls.S3ObjectURL}/${this._accessLevel}/`;
      // PKConstants.GET_TEST_DATA_URL = `assets/jsons/test_data1.json`;
      // PKConstants.GOOGLE_PLACES_API_URL = `${dev_urls.googlePlacesURL}`;
      
    }else{
    

    }
    // if(initGoogleMap)
    //   this.initGoogleMap();
  }
  private initGoogleMap() {
    const win = window as any;
    const google = win.google;

    if (!google) {
      const script = document.createElement("script");
      script.src = PKConstants.GOOGLE_PLACES_API_URL;
      script.async = true;
      script.defer = true;
      document.head.appendChild(script);
    } else {
      console.log("Google autocomplete API already added");
    }
  }
  get isLocalEnv():boolean{
    return this._isLocalEnv;
  }

  set isLocalEnv(value:boolean){
    this._isLocalEnv = value;
  }
}
