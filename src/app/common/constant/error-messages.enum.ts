export enum ErrorCodes {
    AWS_USER_NOT_FOUND = 'UserNotFoundException',
    AWS_USER_NOT_CONFIRMED = 'UserNotConfirmedException',
    AWS_USER_WRONG_CREDENTIALS = 'NotAuthorizedException',
    AWS_INVALID_VALIDATION = 'InvalidParameterException',
    LOGIN_ERROR_HEADING = 'Error signing in',
    REGISTER_ERROR_HEADING = 'Error signing up',
    VERIFICATION_ERROR_HEADING = 'Sorry can\'t verify you',
    FORGOT_PASSWORD_ERROR_HEADER = 'Reset password error!',
    SERVER_ERROR_CODE = 'SERVER ERROR',
    SERVER_ERROR_RESPONSE = 'Server issue occured. Please login again.',
    HTTP_ERROR_HEADING = 'Oops!',
    HTTP_ERROR_MESSAGE = 'Something went wrong! Please try again after sometime.',
    EMPLOYEE_ADD_ERROR = 'Sorry can\'t add employee',
    MENU_CREATION_ERROR_HEADING = 'Can\'t create menu',
    MENU_CREATION_ERROR_MESSAGE = 'Cannot get category details, please try again.',
    INVENTORY_CREATION_ERROR_HEADING = 'Can\'t create product',
    GOOGLE_PLACES_ERROR_MSG = 'Sorry, cannot get proper address details please choose another option.',
    VENDOR_CREATION_ERROR_HEADING = 'Vendor creation error',
    AWS_S3_UPLOAD_ERROR_HEADING = 'Upload error',
    IMAGE_FORMAT_READ_ERROR_MESSAGE = 'Error reading file, unsupported file format.',
    WRONG_PASSWORD_ERROR_MESSAGE = 'You have entered wrong password.',
    INVALID_VALIDATION_ERROR_MESSAGE = 'Your credentials are either not properly validated or doesn\'t satisfy minimum requirements',
    SUBSCRIPTION_ERROR_HEADING = 'Subscription error'
}
